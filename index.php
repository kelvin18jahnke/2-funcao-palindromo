<?php
    require_once './funcoes/palindromo.php';
    $palidromoFunc = new palidromo();
    
    if (isset($_POST['texto'])){
        
        $texto =  $_POST['texto'];
        
        if($texto == ''){
            echo '<div class="alert alert-info" role="alert">
                Digite um texto para verificar se é um Palíndromo!
                </div>';
            
        }else{
            //Funnção que retorna o século do ano.
            $palindromo = $palidromoFunc->VerificaPalindromo($_POST['texto']);  
        }
    }else{
        $texto = '';
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
        <title>Palíndromo:</title>
    </head>
    <body>
       
        <form action="index.php" method="POST">
            <div class="row g-3 align-items-center" style="margin: 2% 10%;">
                <h1>Função que Retorna se é um palíndromo</h1>
                <div class="col-auto">
                    <label  class="col-form-label">Digite um texto:</label>
                </div>
                <div class="col-auto">
                    <input type="text" name="texto" value="<?=$texto?>" class="form-control">
                </div>
                <div class="col-auto">
                    <span  class="form-text">
                       <?php
                        if(isset($palindromo)){
                            if ($palindromo && true){
                                echo 'É um Palíndromo';
                            }else{
                                 echo 'Não é um Palíndromo';
                            }
                           
                        } 
                       ?>
                    </span>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </form>
    </body>
</html>
