<?php

class palidromo {
    public function VerificaPalindromo($palindromo) {
        
        //Número de caracteres da String
        $numeroCaracteres = strlen($palindromo);
        
        //Verifica se a string possui apenas um caracter se possuir já retorna true, caso não possuir faz a segunda verificação
        if($numeroCaracteres == 1){
            $verificaPalindromo = true;
        }else{
           //valor da string de frente para trás
           $polidromo1 = $palindromo;
           //valor da string de trás para frente
           $polidromo2 = strrev($palindromo);
           
           //Verifica se é políndromo
          
           if ($polidromo1 === $polidromo2) {
               $verificaPalindromo = true;
           }else{
               $verificaPalindromo = false;
           }
        }
       
        return $verificaPalindromo;
    }
}
